class login_page
    include Capybara::DSL
    def go
        visit "index.php?"
    end

    def with(username, password)
        find("#email").set username
        find("#passwd").set password
        find(::xpath, "//a[@class='login']").click
    end

    def alert
        find(".alert-danger").text
    end
end