#language: pt

Funcionalidade: Login
    Preciso que seja realizado o login do usuário com sucesso
        Contexto: Dado que faco o login
            Dado que acesso a pagina e clico no botão "login"
        
        @login_sucesso
        Cenario: Acesso com sucesso
             Quando eu faço o login com o usuário "matiassidnei@gmail.com" e insiro a senha "123456"
             Então eu acesso a área segura "My account" com sucesso

        @login_branco
        Cenario: Login invalido
             Quando eu faço o login com o usuário " " e insiro a senha " "
             Então devo receber o alerta "An email address required."
 
        