Dado ('que acesso a pagina e clico no botão {string}') do |login|
  visit "index.php?"
  find(:xpath, "//a[@class='login']").click
end

Quando('eu faço o login com o usuário {string} e insiro a senha {string}') do |username, password|
    find("#email").set username
    find("#passwd").set password
    find("#SubmitLogin").click
    sleep 3
    
end
  
Então('eu acesso a área segura {string} com sucesso') do |expect_name|
  expect(page).to have_text expect_name
end

Então('devo receber o alerta {string}') do |expect_message|
  sleep 4
    expect(find('.alert-danger')).to have_content expect_message    
end

